"""
=====================================================================
Unsupervised Learning: Image Segmentation based on K-Means Clustering
=====================================================================

Author: Haotian Shi, 2017

"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import sys
import itertools
import numpy as np
from PIL import Image

print(__doc__)

# get command line input parameters
input_params = sys.argv
#input_params = [None, 100, 'city_scene.png', '3D_city_scene.png']

K = int(input_params[1]) # cluster number
inputImageFilename = input_params[2]
outputImageFilename = input_params[3]

# generate X with features (r g b i j), each pixel generates one instance
rgb_img = Image.open(inputImageFilename).convert('RGB') # read image in as RGB image
w, h = rgb_img.width, rgb_img.height
indices = np.zeros((w, h), dtype=np.uint32) # backup pixel indices in X
n = w * h
X = np.zeros((n, 5))

idx = 0
for i, j in itertools.product(range(w), range(h)):
    rgb = rgb_img.getpixel((i, j))
    X[idx] = np.array((rgb[0], rgb[1], rgb[2], i, j)) # train instance
    indices[i, j] = idx # backup pixel index, since standardize will make position untraceable
    idx += 1

# start K-Means Clustering
mean = np.mean(X, axis=0) # keep mean and std for future destandardize
std = np.std(X, axis=0)
X = (X - mean) / std # standardize features

E = 1e-2 # threshold for judgement of converage
centroids = X[np.random.choice(n, size=K, replace=False)] # initializing centroids by randomly choosing points
centroid_choices = np.zeros(n, dtype=np.uint8)
while(True):
    updated_centroids = np.zeros((K, 5))
    cluster_counts = np.zeros(K) # point number for each cluster
    for i in range(n):
        point = X[i]
        centroid_idx = 0 # current point final choice of centroid in this loop
        minD = np.inf
        for c in range(K):
            d = np.linalg.norm(point - centroids[c]) # distance from point to centroid
            if(d < minD):
                minD = d
                centroid_idx = c
        updated_centroids[centroid_idx] += point # add current point to its cluster
        cluster_counts[centroid_idx] += 1
        centroid_choices[i] = centroid_idx # keep track of latest centroid choices
    for c in range(K):
        if(cluster_counts[c] == 0): # if not point is in current cluster, randomly assign a point to this cluster
            i = np.random.choice(n, size=1, replace=False)
            updated_centroids[c] = X[i]
            centroid_choices[i] = c
        else:
            updated_centroids[c] /= cluster_counts[c]
    if(np.linalg.norm(updated_centroids - centroids) < E):
        for i in range(n):
            centroid = centroids[centroid_choices[i]]
            X[i] = np.array((centroid[0], centroid[1], centroid[2], X[i][3], X[i][4]))
        break
    centroids = updated_centroids # update centroids
X = X * std + mean # destardardize

# generate the processed RGB image
pixels = np.zeros((h, w, 3), dtype=np.uint8) # save the updated RGB info
for i, j in itertools.product(range(w), range(h)):
    features = X[indices[i, j]]
    pixels[j, i] = np.array((int(features[0]), int(features[1]), int(features[2]))) # updated rgb

rgb_3d_img = Image.fromarray(pixels, 'RGB') # the shape of array for Image.fromarray should be (h, w, 3)
rgb_3d_img.save(outputImageFilename)
